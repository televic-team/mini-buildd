;; Developer setup for mini-buildd HTML templates
(when (require 'web-mode nil 'noerror) (add-to-list 'auto-mode-alist '("\\.html\\'" . web-mode)))
(setq web-mode-engines-alist '(("django" . "\\.html\\'")))
(defun mbd-web-mode-hook ()
	(setq web-mode-markup-indent-offset 2)
	(setq web-mode-css-indent-offset 2)
	(setq web-mode-style-padding 2)
	(setq web-mode-script-padding 2)
	(setq web-mode-code-indent-offset 2)
)
(add-hook 'web-mode-hook  'mbd-web-mode-hook)
