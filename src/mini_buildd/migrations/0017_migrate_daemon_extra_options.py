# Generated by Django 4.2.9 on 2024-01-07 16:14

from django.db import migrations

from . import mbd_get_extra_option


def copy_extra_options(apps, _schema_editor):
    Daemon = apps.get_model("mini_buildd", "Daemon")
    for daemon in Daemon.objects.all():
        daemon.custom_archive_origin = mbd_get_extra_option(daemon, "X-Archive-Origin", "")
        daemon.save()


class Migration(migrations.Migration):

    dependencies = [
        ('mini_buildd', '0016_add_daemon_extra_options'),
    ]

    operations = [
        migrations.RunPython(copy_extra_options),
    ]
