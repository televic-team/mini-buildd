# Generated by Django 4.2.9 on 2024-01-06 14:31

from django.db import migrations

from . import mbd_get_extra_options, mbd_get_extra_option


def copy_extra_options(apps, _schema_editor):
    Source = apps.get_model("mini_buildd", "Source")
    for source in Source.objects.all():
        source.extra_identifiers = {k: v for k, v in mbd_get_extra_options(source).items() if not k.startswith("X-")}  # Keep "X-<header>" for special purposes. All other keys are like in a Release file.
        source.check_valid_until = mbd_get_extra_option(source, "X-Check-Valid-Until", "yes").lower() in ("yes", "true", "1")
        source.remove_from_component = mbd_get_extra_option(source, "X-Remove-From-Component", "")
        source.save()


class Migration(migrations.Migration):

    dependencies = [
        ('mini_buildd', '0009_add_source_extra_options'),
    ]

    operations = [
        migrations.RunPython(copy_extra_options),
    ]
