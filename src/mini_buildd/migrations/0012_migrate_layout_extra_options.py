# Generated by Django 4.2.9 on 2024-01-06 17:48

from django.db import migrations


from . import mbd_get_extra_option


def copy_extra_options(apps, _schema_editor):
    Layout = apps.get_model("mini_buildd", "Layout")
    for layout in Layout.objects.all():
        for kv in mbd_get_extra_option(layout, "Meta-Distributions", "").split():
            k, v = kv.split("=")
            layout.meta_distributions[k] = v
        layout.save()


class Migration(migrations.Migration):

    dependencies = [
        ('mini_buildd', '0011_add_layout_extra_options'),
    ]

    operations = [
        migrations.RunPython(copy_extra_options),
    ]
