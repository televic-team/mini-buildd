# Historical "extra options" support (copied at some point from base model ``mini_buildd.Model``)
# * Model methods can't be used in migrations anyway
# * Eventually, "extra options" support will be removed completely, but will still be needed for migrations
def mbd_get_extra_options(obj):
    """In case there are multiple entries, the last wins"""
    result = {}
    for line in obj.extra_options.splitlines():
        lkey, _lsep, lvalue = line.partition(":")
        if lkey:
            result[lkey] = lvalue.lstrip()
    return result


def mbd_get_extra_option(obj, key, default=None):
    return mbd_get_extra_options(obj).get(key, default)
