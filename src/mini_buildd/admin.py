"""
By django convention this module is used to register models for the admin site

See: https://docs.djangoproject.com/en/3.1/ref/contrib/admin/#django.contrib.admin.autodiscover
"""
import django.contrib

from mini_buildd import models

MODELS = [
    models.AptKey,
    models.Uploader,
    models.Remote,
    models.Archive,
    models.Architecture,
    models.Component,
    models.Source,
    models.PrioritySource,
    models.EmailAddress,
    models.Suite,
    models.Layout,
    models.Distribution,
    models.Repository,
    models.Chroot,
    models.DirChroot,
    models.FileChroot,
    models.LVMChroot,
    models.LoopLVMChroot,
    models.BtrfsSnapshotChroot,
    models.Daemon,
    models.Subscription,
]


class AdminSite(django.contrib.admin.sites.AdminSite):
    index_template = "admin/mini_buildd.html"

    __APP_DATA = {
        "mini_buildd": {"order": 1, "setup": "setup"},
        "auth": {"order": 2},
    }
    __MODEL_DATA = {
        # Daemon
        "Daemon": {
            "class_path": "Daemon",
            "header": "Daemon",
            "order": 0,
        },
        # Sources
        "Archive": {
            "class_path": "Archive",
            "header": "Sources",
            "order": 10,
        },
        "Source": {
            "class_path": "Source",
            "order": 11,
        },
        "PrioritySource": {
            "class_path": "PrioritySource",
            "order": 12,
        },
        "AptKey": {
            "class_path": "AptKey",
            "order": 13,
        },
        # Repositories
        "Layout": {
            "class_path": "Layout",
            "header": "Repositories",
            "order": 20,
        },
        "Distribution": {
            "class_path": "Distribution",
            "order": 21,
        },
        "Repository": {
            "class_path": "Repository",
            "order": 22,
        },
        "Uploader": {
            "class_path": "Uploader",
            "order": 23,
        },
        # Chroots
        "DirChroot": {
            "class_path": "DirChroot",
            "header": "Chroots",
            "order": 30,
        },
        "FileChroot": {
            "class_path": "FileChroot",
            "order": 31,
        },
        "LVMChroot": {
            "class_path": "LVMChroot",
            "order": 32,
        },
        "LoopLVMChroot": {
            "class_path": "LoopLVMChroot",
            "order": 33,
        },
        "BtrfsSnapshotChroot": {
            "class_path": "BtrfsSnapshotChroot",
            "order": 34,
        },
        # Remotes
        "Remote": {
            "class_path": "Remote",
            "header": "Remotes",
            "order": 40,
        },
        # Others
        "Architecture": {
            "class_path": "Architecture",
            "header": "Others",
            "order": 50,
        },
        "Component": {
            "class_path": "Component",
            "order": 51,
        },
        "EmailAddress": {
            "class_path": "EmailAddress",
            "order": 52,
        },
        "Suite": {
            "class_path": "Suite",
            "order": 53,
        },
        "Subscription": {
            "class_path": "Subscription",
            "order": 54,
        },
        "Chroot": {
            "class_path": "Chroot",
            "order": 55,
        },
    }

    def __init__(self):
        super().__init__("mini-buildd-admin")
        # Default action 'delete_selected' action does not call
        # custom delete.
        #
        # See: https://docs.djangoproject.com/en/dev/ref/contrib/admin/actions/
        #
        # So we just disable this default action. You can still delete
        # single objects from the model's form.
        self.disable_action("delete_selected")

        for m in MODELS:
            admin_class = getattr(m, "Admin")
            self.register(m, admin_class)
            self._registry.update(django.contrib.admin.site._registry)  # Add other apps that have been autodiscovered

    def get_app_list(self, request, app_label=None):  # pylint: disable=unused-argument  # Needed for django < 4.1 -- 4.1 added 'app_label=None'
        """Proper order && group header for mini_buildd app; others apps still sorted alphabetically"""
        app_dict = self._build_app_dict(request)
        app_list = sorted(app_dict.values(), key=lambda app: self.__APP_DATA[app["app_label"]].get("order", 999))
        for app in app_list:
            if app["app_label"] == "mini_buildd":
                app["models"].sort(key=lambda model: self.__MODEL_DATA[model["object_name"]].get("order", 999))
                app["mbd"] = self.__APP_DATA["mini_buildd"]

                for model in app["models"]:
                    model["mbd"] = self.__MODEL_DATA[model["object_name"]]
            else:
                app['models'].sort(key=lambda model: model['name'])

        return app_list


site = AdminSite()
