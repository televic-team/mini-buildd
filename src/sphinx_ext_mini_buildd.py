import docutils.nodes
import docutils.parsers.rst


class Example(docutils.parsers.rst.Directive):
    def run(self):
        paragraph_node = docutils.nodes.paragraph(text="Example")
        return [paragraph_node]


def setup(app):
    app.add_directive("mbd-example", Example)
    return {
        "version": "0.1",
        "parallel_read_safe": True,
        "parallel_write_safe": True,
    }
